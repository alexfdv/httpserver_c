.PHONY: clean All

All:
	@echo "----------Building project:[ HttpServer - Debug ]----------"
	@cd "HttpServer" && "$(MAKE)" -f  "HttpServer.mk"
clean:
	@echo "----------Cleaning project:[ HttpServer - Debug ]----------"
	@cd "HttpServer" && "$(MAKE)" -f  "HttpServer.mk" clean
