#include "http_handlers.h"

#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#include "http_transport.h"
#include "utils.h"
#include "log.h"

static char* validate_url(const config_t* conf, const char* url)
{
    char* file_path = NULL;

    if (strstr(url, "../")) {
        /* wrong url */
        return NULL;
    }

    if (0 == strcmp(url, "/")) {
        file_path = strdup(conf->default_document_path);
    } else {
        file_path = strings_concat(conf->root_folder, url);
    }

    return file_path;
}

int handle_get_request(const client_t* client, const http_request_t* request, const config_t* conf)
{
    int ret = -1;

    /* create response struct */
    http_response_t* response = http_response_new(OK);
    if (!response)
        goto exit;

    char* file_path = validate_url(conf, request->url);
    if (file_path == NULL) {
        ret = -2;
        goto exit;
    }

    response->payload_fd = open(file_path, O_RDONLY);
    if (response->payload_fd <= 0) {
        ret = -3;
        goto exit;
    }

    /* commented: browser will decide what it is by itself */
    //ret = http_add_response_property(response, "Content-Type", "text/html;charset=utf-8");
    //if (ret < 0)
    //    goto exit;

    /* no compression */
    ret = http_add_response_property(response, "Content-Encoding", "identity");
    if (ret < 0)
        goto exit;

    ret = http_send_response(client, response);
    if (ret < 0)
        goto exit;

exit:
    free(file_path);
    http_response_free(response);

    return ret;
}

int handle_default(const client_t* client)
{
    return http_send_error_response(client, NOT_IMPLEMENTED);
}
