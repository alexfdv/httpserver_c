#ifndef UTILS_INCLUDE_
#define UTILS_INCLUDE_

#include <stdbool.h>

#include "http_protocol.h"

/**
 * @brief Returns file size for file descriptor fd
 * @param fd - openned file descriptor
 * @return not negative number on success and -1 in case of error
 */
int get_file_size(int fd);

/**
 * @brief Concatenates two strings. A new string should be free'd by a caller
 * @param first - first string to concatenate
 * @param second - second string to concatenate
 * @return a new string on success. Returns NULL on error
 */
char* strings_concat(const char* first, const char* second);

/**
 * @brief Checks that 'folder' exists
 * @param folder - path to folder
 * @return 'true' if folder exists and 'false' if folder doesn't exist
 */
bool check_folder_exists(const char* folder);

/**
 * @brief Checks that file (pointed by 'filepath') exists
 * @param filepath - path to file
 * @return 'true' if file exists and 'false' if file doesn't exist
 */
bool check_file_exists(const char* filepath);

/**
 * @brief Converts 'method' numberic value to pointer to a string literal.
 * @param method - numeric value from http_method_e enum type
 * @return pointer to a string literal
 */
char* http_method_to_string(http_method_e method);

#endif  // UTILS_INCLUDE_
