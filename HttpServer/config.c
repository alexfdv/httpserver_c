#include "config.h"

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"


static int skip_newlines(char* buffer, int length)
{
    int chars_read = 0;

    while (chars_read < length) {
        char c = *buffer;

        /* Stop at non-whitespace */
        if (c != '\n' && c != '\t' && c!=' ')
            break;

        chars_read++;
        buffer++;
    }

    return chars_read;
}



static int parse_line(char* buffer, int length, config_t* conf)
{
    if (length == 0) {
        return 0;
    }

    /* skip new line, spaces and tabs*/
    int ret = skip_newlines(buffer, length);
    if (ret > 0) {
        return ret;
    }

    char* equal_sign = memchr(buffer, '=', length);
    if (equal_sign == NULL) {
        return -1;
    }

    char* end_line = memchr(equal_sign, '\n', length);
    if (end_line == NULL) {
        return -1;
    }

    char* key = buffer;
    char* value = equal_sign + 1;

    int key_len = (int)(equal_sign - buffer);
    int value_len = (int)(end_line - equal_sign - 1);

    if (0 == memcmp(key, "port", key_len)) {
        free(conf->port);  // delete default value
        conf->port = strndup(value, value_len);
    } else if (0 == memcmp(key, "document", key_len)) {
        free(conf->default_document); // delete default value
        conf->default_document = strndup(value, value_len);
    } else if (0 == memcmp(key, "root_folder", key_len)) {
        free(conf->root_folder); // delete default value
        conf->root_folder = strndup(value, value_len);
    } else if (0 == memcmp(key, "interface", key_len)) {
        free(conf->interface); // delete default value
        conf->interface = strndup(value, value_len);
    } else if (0 == memcmp(key, "logging", key_len)) {
        if (0 == memcmp(value, "on", value_len)) {
            conf->enabled_logging = true;
        } else {
            conf->enabled_logging = false;
        }
    }

    return end_line - buffer + 1;
}

static int parse_config(config_t* conf, int fd)
{
    char buffer[100];

    char* line_start = buffer;
    int length = 0;
    int parsed = 0;

    int chars_read;

    /* Read from buffer */
    while ((chars_read = read(fd, buffer + length, sizeof(buffer) -  length)) > 0) {

        length += chars_read;

        /* where to start parsing*/
        line_start = buffer;

        /* Parse the line until it is possible */
        while ((parsed = parse_line(line_start, length, conf)) > 0) {
            line_start += parsed;
            length -= parsed;
        }

        /* Shift to front */
        memmove(buffer, line_start, length);

    }

    /* build full path at the end */
    conf->default_document_path = (char*)strings_concat(conf->root_folder, conf->default_document);

    if (parsed < 0) {
        return -1;
    }

    if (chars_read < 0) {
        return -1;
    }

    /* Read successfully */
    return 0;
}

static config_t* config_new()
{
    config_t* conf = malloc(sizeof(config_t));
    if (conf == NULL)
        return NULL;

    /* load default values */
    conf->port = strdup("8080");
    conf->enabled_logging = true;
    conf->default_document = strdup("index.html");
    conf->root_folder = strdup("./");
    conf->default_document_path = strings_concat(conf->root_folder, conf->default_document);
    conf->interface = NULL;

    return conf;
}

static void config_free(config_t* conf)
{
    if (conf == NULL)
        return;

    free(conf->port);
    free(conf->default_document);
    free(conf->root_folder);
    free(conf->default_document_path);
    free(conf->interface);
    free(conf);
}

config_t* load_config(char* file_path)
{
    config_t* conf = config_new();
    if (conf == NULL)
        goto exit;


    int fd = open(file_path, O_RDONLY);
    if (fd > 0) {
        int ret = parse_config(conf, fd);

        close(fd);

        if (ret != 0) {
            fprintf(stderr,"Unable to parse %s\n", file_path);
            goto error;
        }

    } else {
        fprintf(stderr,"Could not open configuration file: %s\n", file_path);
        goto error;
    }

exit:
    return conf;
error:
    config_free(conf);
    return NULL;

}
