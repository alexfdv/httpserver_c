##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=HttpServer
ConfigurationName      :=Debug
WorkspaceConfiguration := $(ConfigurationName)
WorkspacePath          :=/home/alex/Documents/HttpServer
ProjectPath            :=/home/alex/Documents/HttpServer/HttpServer
IntermediateDirectory  :=../build-$(ConfigurationName)/HttpServer
OutDir                 :=../build-$(ConfigurationName)/HttpServer
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=alex
Date                   :=23/12/19
CodeLitePath           :=/home/alex/.codelite
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../build-$(ConfigurationName)/bin/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :=$(IntermediateDirectory)/ObjectsList.txt
PCHCompileFlags        :=
LinkOptions            :=  -pthread
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=../build-$(ConfigurationName)/HttpServer/main.c$(ObjectSuffix) ../build-$(ConfigurationName)/HttpServer/config.c$(ObjectSuffix) ../build-$(ConfigurationName)/HttpServer/http_protocol.c$(ObjectSuffix) ../build-$(ConfigurationName)/HttpServer/log.c$(ObjectSuffix) ../build-$(ConfigurationName)/HttpServer/http_transport.c$(ObjectSuffix) ../build-$(ConfigurationName)/HttpServer/http_handlers.c$(ObjectSuffix) ../build-$(ConfigurationName)/HttpServer/network.c$(ObjectSuffix) ../build-$(ConfigurationName)/HttpServer/utils.c$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: MakeIntermediateDirs $(OutputFile)

$(OutputFile): ../build-$(ConfigurationName)/HttpServer/.d $(Objects) 
	@mkdir -p "../build-$(ConfigurationName)/HttpServer"
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@mkdir -p "../build-$(ConfigurationName)/HttpServer"
	@mkdir -p ""../build-$(ConfigurationName)/bin""

../build-$(ConfigurationName)/HttpServer/.d:
	@mkdir -p "../build-$(ConfigurationName)/HttpServer"

PreBuild:


##
## Objects
##
../build-$(ConfigurationName)/HttpServer/main.c$(ObjectSuffix): main.c ../build-$(ConfigurationName)/HttpServer/main.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/alex/Documents/HttpServer/HttpServer/main.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/HttpServer/main.c$(DependSuffix): main.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/HttpServer/main.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/HttpServer/main.c$(DependSuffix) -MM main.c

../build-$(ConfigurationName)/HttpServer/main.c$(PreprocessSuffix): main.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/HttpServer/main.c$(PreprocessSuffix) main.c

../build-$(ConfigurationName)/HttpServer/config.c$(ObjectSuffix): config.c ../build-$(ConfigurationName)/HttpServer/config.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/alex/Documents/HttpServer/HttpServer/config.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/config.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/HttpServer/config.c$(DependSuffix): config.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/HttpServer/config.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/HttpServer/config.c$(DependSuffix) -MM config.c

../build-$(ConfigurationName)/HttpServer/config.c$(PreprocessSuffix): config.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/HttpServer/config.c$(PreprocessSuffix) config.c

../build-$(ConfigurationName)/HttpServer/http_protocol.c$(ObjectSuffix): http_protocol.c ../build-$(ConfigurationName)/HttpServer/http_protocol.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/alex/Documents/HttpServer/HttpServer/http_protocol.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/http_protocol.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/HttpServer/http_protocol.c$(DependSuffix): http_protocol.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/HttpServer/http_protocol.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/HttpServer/http_protocol.c$(DependSuffix) -MM http_protocol.c

../build-$(ConfigurationName)/HttpServer/http_protocol.c$(PreprocessSuffix): http_protocol.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/HttpServer/http_protocol.c$(PreprocessSuffix) http_protocol.c

../build-$(ConfigurationName)/HttpServer/log.c$(ObjectSuffix): log.c ../build-$(ConfigurationName)/HttpServer/log.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/alex/Documents/HttpServer/HttpServer/log.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/log.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/HttpServer/log.c$(DependSuffix): log.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/HttpServer/log.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/HttpServer/log.c$(DependSuffix) -MM log.c

../build-$(ConfigurationName)/HttpServer/log.c$(PreprocessSuffix): log.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/HttpServer/log.c$(PreprocessSuffix) log.c

../build-$(ConfigurationName)/HttpServer/http_transport.c$(ObjectSuffix): http_transport.c ../build-$(ConfigurationName)/HttpServer/http_transport.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/alex/Documents/HttpServer/HttpServer/http_transport.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/http_transport.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/HttpServer/http_transport.c$(DependSuffix): http_transport.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/HttpServer/http_transport.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/HttpServer/http_transport.c$(DependSuffix) -MM http_transport.c

../build-$(ConfigurationName)/HttpServer/http_transport.c$(PreprocessSuffix): http_transport.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/HttpServer/http_transport.c$(PreprocessSuffix) http_transport.c

../build-$(ConfigurationName)/HttpServer/http_handlers.c$(ObjectSuffix): http_handlers.c ../build-$(ConfigurationName)/HttpServer/http_handlers.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/alex/Documents/HttpServer/HttpServer/http_handlers.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/http_handlers.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/HttpServer/http_handlers.c$(DependSuffix): http_handlers.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/HttpServer/http_handlers.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/HttpServer/http_handlers.c$(DependSuffix) -MM http_handlers.c

../build-$(ConfigurationName)/HttpServer/http_handlers.c$(PreprocessSuffix): http_handlers.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/HttpServer/http_handlers.c$(PreprocessSuffix) http_handlers.c

../build-$(ConfigurationName)/HttpServer/network.c$(ObjectSuffix): network.c ../build-$(ConfigurationName)/HttpServer/network.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/alex/Documents/HttpServer/HttpServer/network.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/network.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/HttpServer/network.c$(DependSuffix): network.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/HttpServer/network.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/HttpServer/network.c$(DependSuffix) -MM network.c

../build-$(ConfigurationName)/HttpServer/network.c$(PreprocessSuffix): network.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/HttpServer/network.c$(PreprocessSuffix) network.c

../build-$(ConfigurationName)/HttpServer/utils.c$(ObjectSuffix): utils.c ../build-$(ConfigurationName)/HttpServer/utils.c$(DependSuffix)
	$(CC) $(SourceSwitch) "/home/alex/Documents/HttpServer/HttpServer/utils.c" $(CFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/utils.c$(ObjectSuffix) $(IncludePath)
../build-$(ConfigurationName)/HttpServer/utils.c$(DependSuffix): utils.c
	@$(CC) $(CFLAGS) $(IncludePath) -MG -MP -MT../build-$(ConfigurationName)/HttpServer/utils.c$(ObjectSuffix) -MF../build-$(ConfigurationName)/HttpServer/utils.c$(DependSuffix) -MM utils.c

../build-$(ConfigurationName)/HttpServer/utils.c$(PreprocessSuffix): utils.c
	$(CC) $(CFLAGS) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) ../build-$(ConfigurationName)/HttpServer/utils.c$(PreprocessSuffix) utils.c


-include ../build-$(ConfigurationName)/HttpServer//*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(IntermediateDirectory)


