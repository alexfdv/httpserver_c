#ifndef HTTP_TRANSPORT_INCLUDE_
#define HTTP_TRANSPORT_INCLUDE_

#include "http_protocol.h"
#include "network.h"

/**
 * @brief Sends a response to a client
 * @param client - connected client
 * @param response - response to send
 * @return 0 on success
 */
int http_send_response(const client_t* client, http_response_t* response);

/**
 * @brief Sends error response
 * @param client - connected client
 * @param code - error code
 * @return 0 on success
 */
int http_send_error_response(const client_t* client, int code);

#endif //HTTP_TRANSPORT_INCLUDE_
