#ifndef HTTP_PROTOCOL_INCLUDE_
#define HTTP_PROTOCOL_INCLUDE_

#include <stdbool.h>

#define CRLF "\r\n"
#define PAYLOAD_DELIMITER CRLF CRLF
#define PAYLOAD_DELIMITER_LEN 4

enum {
    UNKNOWN = 0
};

typedef enum http_method {
    GET = 1,
    HEAD,
    POST,
    PUT,
    DELETE,
    CONNECT,
    OPTIONS,
    TRACE,
    PATCH
} http_method_e;

enum http_response_code {
    OK              = 200,
    BAD_REQUEST     = 400,
    NOT_FOUND       = 404,
    NOT_IMPLEMENTED = 501,
};

// TODO: add other fields
typedef struct {
    enum http_method method;
    bool keep_alive;
    char* url;
} http_request_t;

// TODO: move 'payload' fields to a separate struct that can be fd, array or another type.
typedef struct {
    enum http_response_code code;
    int payload_fd;
    char* header;
    char* properties;
} http_response_t;

/**
 * @brief Tries to parse request_buffer. Request buffer can contain one or more requests at a time.
 * @param request_buffer - received buffer (with request inside)
 * @param request - putput parameter
 * @return number of bytes consumed in buffer after parsing.
 */
int try_parse_http_request(const char* request_buffer, http_request_t* request);

/**
 * @brief Creates a new response struct.
 * @param code - response code
 * @return not NULL on success. Returns NULL on error.
 */
http_response_t* http_response_new(int code);

/**
 * @brief Adds a new key-value HTTP property to the response object.
 * @param response - output parameter with new property
 * @param key - key of a property
 * @param value - value of a property
 * @return 0 on success
 */
int http_add_response_property(http_response_t* response, char* key, char* value);

/**
 * @brief Removes request struct and frees all underlying data
 * @param request - struct to remove
 */
void http_request_free(http_request_t* request);

/**
 * @brief Removes response struct and frees all underlying data
 * @param response - struct to remove
 */
void http_response_free(http_response_t* response);


#endif // HTTP_PROTOCOL_INCLUDE_
