#include "http_protocol.h"

#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int try_parse_http_request(const char* request_buffer, http_request_t* request)
{
    /* Ignore first CRLF if exists */
    if (0 == strncmp(request_buffer, "\r\n", 2)) {
        request_buffer = request_buffer + 2;
    }
    
    char* request_end = strstr(request_buffer, CRLF CRLF);
    if (request_end == NULL) {
        /* could not find end of header, need more data */
        return 0;
    }

    if (0 == strncmp(request_buffer, "GET", 3)) {
        request->method = GET;
    } else if (0 == strncmp(request_buffer, "HEAD", 4)) {
        request->method = HEAD;
    } else if (0 == strncmp(request_buffer, "POST", 4)) {
        request->method = POST;
    } else if (0 == strncmp(request_buffer, "PUT", 3)) {
        request->method = PUT;
    } else if (0 == strncmp(request_buffer, "DELETE", 6)) {
        request->method = DELETE;
    } else if (0 == strncmp(request_buffer, "CONNECT", 7)) {
        request->method = CONNECT;
    } else if (0 == strncmp(request_buffer, "OPTIONS", 7)) {
        request->method = OPTIONS;
    } else if (0 == strncmp(request_buffer, "TRACE", 5)) {
        request->method = TRACE;
    } else if (0 == strncmp(request_buffer, "PATCH", 5)) {
        request->method = PATCH;
    } else {
        /* wrong http header */
        request->method = UNKNOWN;
        return -1;
    }
    


    request->url = NULL;

    /* read url path */
    char* url_start = strchr(request_buffer, ' ');
    if (!url_start) {
        return -1;
    }
    char* url_end = strchr(url_start + 1, ' ');
    if (!url_end) {
        return -1;
    }

    char* end_of_first_line = strchr(request_buffer, '\n');
    if (end_of_first_line <= url_end) {
        /* wrong format of the first line*/
        return -1;
    }

    int url_len = url_end - url_start;
    request->url = (char*)malloc(url_len);
    if (request->url == NULL) {
        return -1;
    }

    memcpy(request->url, url_start + 1, url_len - 1);
    request->url[url_len-1] = '\0';   // set last char as \0
    /* end read url path */
    
    /* keep-alive parsing */
    request->keep_alive = true;
    if (!strstr(request_buffer, "Connection: keep-alive")) {
        request->keep_alive = false;
    }

    return (int)(request_end - request_buffer) + PAYLOAD_DELIMITER_LEN;
}

void http_request_free(http_request_t* request)
{
    if (request->url)
        free(request->url);
}

static char* get_header(int code)
{
    char* result = NULL;
    
    switch (code) {
        case OK:
            return "HTTP/1.1 200 OK";
        case BAD_REQUEST:
            return "HTTP/1.1 400 Bad Request";
        case NOT_FOUND:
            return "HTTP/1.1 404 Not Found";
        case NOT_IMPLEMENTED:
            return "HTTP/1.1 501 Not Implemented";
    }
    
    return result;
}

int http_add_response_property(http_response_t* response, char* key, char* value)
{
    int prop_size = 0;
    if (response->properties != NULL) {
        prop_size = strlen(response->properties);
    }
    prop_size += strlen(key);
    prop_size += 2; // ': ' (colon with space)
    prop_size += strlen(value);
    prop_size += 1; // end line \n
    prop_size += 1; // final \0

    if (response->properties == NULL) {
        response->properties = malloc(prop_size);
        memset(response->properties, 0, prop_size);
    }
    else {
        response->properties = realloc(response->properties, prop_size);
    }

    if (!response->properties) {
        return -1;
    }
    
    strcat(response->properties, "\n");
    strcat(response->properties, key);
    strcat(response->properties, ": ");
    strcat(response->properties, value);
    
    /*strcat adds terminate null byte automagically*/
    
    return 0;
}

http_response_t* http_response_new(int code)
{
    http_response_t* response = malloc(sizeof(http_response_t));
    if (response == NULL)
        return NULL;
    response->code = code;
    response->header = get_header(code);
    response->payload_fd = -1;
    response->properties = NULL;

    return response;
}

void http_response_free(http_response_t* response)
{
    if (!response) {
        return;
    }
    
    /* close owned fd */
    if (response->payload_fd > 0) {
        close(response->payload_fd);
        response->payload_fd = -1;
    }
    
    if (response->properties) {
        free(response->properties);
        response->properties = NULL;
    }
    
    free(response);
}
