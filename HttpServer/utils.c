#include "utils.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "http_protocol.h"

int get_file_size(int fd)
{
    struct stat st;
    if (fstat(fd, &st) == -1) {
        return -1;
    }

    return st.st_size;
}

char* strings_concat(const char* first, const char* second)
{
    char* res = NULL;

    if (first == NULL || second == NULL)
        return NULL;

    int len_first = strlen(first);
    int len_second = strlen(second);
    int total_len = len_first + len_second + 1;  // with final \0

    res = (char*) malloc(total_len);
    memset(res, 0, total_len);
    strcat(res, first);
    strcat(res, second);

    return res;
}

bool check_folder_exists(const char* folder)
{
    struct stat sb;
    return stat(folder, &sb) == 0 && S_ISDIR(sb.st_mode);
}

bool check_file_exists(const char* filepath)
{
    return (access( filepath, F_OK ) != -1);
}

char* http_method_to_string(http_method_e method)
{
    switch (method) {
    case GET:
        return "GET";
    case POST:
        return "POST";
    case HEAD:
        return "HEAD";
    case PUT:
        return "PUT";
    case DELETE:
        return "DELETE";
    case CONNECT:
        return "CONNECT";
    case OPTIONS:
        return "OPTIONS";
    case TRACE:
        return "TRACE";
    case PATCH:
        return "PATCH";
    default:
        return "unknown";
    }
}
