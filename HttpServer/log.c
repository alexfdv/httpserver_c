#include "log.h"

#include <stdarg.h>
#include <stdio.h>
#include <syslog.h>
#include <time.h>

#include "http_protocol.h"
#include "utils.h"

/* current logging level */
int log_level = INFO_LOG;

// TODO: add synchronization.
// TODO2: improve w3c logging format: more details and log to file(s).
static void log_message(log_level_e level, const char* format,
                        va_list args)
{
    const char* priority_name;
    int priority;

    char message[2048];

    if (level > log_level)
        return;

    vsnprintf(message, sizeof(message), format, args);

    /* Convert log level to syslog priority */
    switch (level) {
    case ERROR_LOG:
        priority = LOG_ERR;
        priority_name = "ERROR";
        break;
    case WARNING_LOG:
        priority = LOG_WARNING;
        priority_name = "WARNING";
        break;
    case INFO_LOG:
        priority = LOG_INFO;
        priority_name = "INFO";
        break;
    case DEBUG_LOG:
        priority = LOG_DEBUG;
        priority_name = "DEBUG";
        break;
    case TRACE_LOG:
        priority = LOG_DEBUG;
        priority_name = "TRACE";
        break;
    default:
        priority = LOG_INFO;
        priority_name = "UNKNOWN";
        break;
    }

    /* Log to syslog */
    syslog(priority, "%s", message);

    if (level == ERROR_LOG) {
        fprintf(stderr, "%s:\t%s\n", priority_name, message);
    } else {
        fprintf(stdout, "%s:\t%s\n", priority_name, message);

    }
}

void write_log(log_level_e level, const char* format, ...)
{
    va_list args;
    va_start(args, format);
    log_message(level, format, args);
    va_end(args);
}

// TODO: write log to file possibility
void write_access_log(const char* source, const char* destination, const http_request_t* request)
{
    if (log_level == 0) {
        return;
    }
    
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    fprintf(stdout, "%d-%d-%d %d:%d:%d %s-%s %s %s\n",
            tm.tm_year + 1900,
            tm.tm_mon + 1,
            tm.tm_mday,
            tm.tm_hour,
            tm.tm_min,
            tm.tm_sec,
            source,
            destination,
            http_method_to_string(request->method),
            request->url);
}
