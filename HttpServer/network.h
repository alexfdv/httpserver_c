#ifndef NETWORK_INCLUDE_
#define NETWORK_INCLUDE_

#include <netinet/in.h>

#include "config.h"

typedef struct {
    int listen_fd;
} server_t;

typedef struct {
    int fd;
    char client_address[INET6_ADDRSTRLEN];
    char host_address[INET6_ADDRSTRLEN];
    uint16_t client_port;
    uint16_t host_port;
} client_t;

/**
 * @brief Initializes a server_t struct by socket creation and setting appropriate options.
 * @param server - output parameter
 * @param config - configuration file
 * @return 0 on success
 */
int socket_init(server_t* server, const config_t* config);

/**
 * @brief Accepts a new connection.
 * @param server - created server using socket_init function
 * @param client - output parameter
 * @return 0 on success
 */
int socket_accept(const server_t* server, client_t* client);

/**
 * @brief Reads new data from the openned connection.
 * @param client - connected client
 * @param buff - output buffer
 * @param len - buffer lenght
 * @return positive number of received bytes on success.
 */
int socket_recv(const client_t* client, char* buff, int len);

#endif // NETWORK_INCLUDE_
