#ifndef HTTP_HANDLERS_INCLUDE_
#define HTTP_HANDLERS_INCLUDE_

#include "http_protocol.h"
#include "network.h"

/**
 * @brief Handles a get request. Sends a response to the client.
 * @param client - connected client.
 * @param request - received request from the client.
 * @param conf - configuration file
 * @return 0 on success
 */
int handle_get_request(const client_t* client, const http_request_t* request, const config_t* conf);

/**
 * @brief Default handler (if couldn't find appropriate)
 * @param client - connected client
 * @return 0 on success
 */
int handle_default(const client_t* client);


#endif // HTTP_HANDLERS_INCLUDE_
