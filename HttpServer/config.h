#ifndef CONFIG_INCLUDE_
#define CONFIG_INCLUDE_

#include <stdbool.h>

typedef struct _config {
    char* port;
    char* default_document;
    char* root_folder;
    char* default_document_path;
    char* interface;
    bool enabled_logging;
} config_t;

/**
 * @brief Loads configuration
 * @param file_path - path to the configuration file
 * @return new configuration struct. Returns NULL on error.
 */
config_t* load_config(char* file_path);

#endif // CONFIG_INCLUDE_
