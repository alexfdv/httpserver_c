#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "http_transport.h"
#include "http_protocol.h"
#include "log.h"
#include "network.h"
#include "utils.h"

static int http_send_response_header(const client_t* connection, const http_response_t* response)
{
    int err = send(connection->fd, response->header, strlen(response->header), 0);
    if (err < 0)
        return err;

    if (response->properties) {
        err = send(connection->fd, response->properties, strlen(response->properties), 0);
        if (err < 0)
            return err;
    }

    err = send(connection->fd, PAYLOAD_DELIMITER, PAYLOAD_DELIMITER_LEN, 0);
    if (err < 0)
        return err;

    return 0;
}

static int http_send_response_normal(const client_t* connection, const http_response_t* response, int size)
{
    int ret = 0;
    char* payload_buffer= NULL;

    if (size <= 0)
        return -1;

    /* send headers first*/
    ret = http_send_response_header(connection, response);
    if (ret < 0)
        goto exit;

    /* send payload */
    if (response->payload_fd > 0 && size > 0) {

        /* read payload from the source */
        payload_buffer = malloc(size);
        if (!payload_buffer)
            return -1;

        ret = read(response->payload_fd, payload_buffer, size);
        if (ret < 0)
            goto exit;

        ret = send(connection->fd, payload_buffer, size, 0);
        if (ret < 0)
            goto exit;
    }
exit:

    free(payload_buffer);
    return ret;
}

static int http_send_response_chunked(const client_t* connection, const http_response_t* response)
{
    /*
     * HTTP/1.1 200 OK
     * Content-Type: text/plain
     * Transfer-Encoding: chunked
     *
     * B\r\n
     * GlobalLogic\r\n
     * 8\r\n
     * test app\r\n
     * 0\r\n
     * \r\n
     */

    char* buffer = NULL;
    const int buf_size = 10000;

    int err = 0;
    int ret = 0;

    http_send_response_header(connection, response);

    buffer = malloc(buf_size);
    memset(buffer, 0, buf_size);

    while ((err = ret = read(response->payload_fd, buffer, buf_size)) > 0) {
        /* size of chunk should be in hex */
        char size_str[10];
        snprintf(size_str, 10, "%x" CRLF, ret);

        err = send(connection->fd, size_str, strlen(size_str), 0);
        if (err < 0)
            goto exit;
        err = send(connection->fd, buffer, ret, 0);
        if (err < 0)
            goto exit;
        err = send(connection->fd, CRLF, strlen(CRLF), 0);
        if (err < 0)
            goto exit;

        /* final part was sent*/
        if (ret < buf_size) {
            break;
        }
    }

    /* final chunk should be 0 */
    err = send(connection->fd, "0" CRLF CRLF, strlen("0" CRLF CRLF), 0);

exit:
    free(buffer);

    return err;
}

int http_send_error_response(const client_t* client, int code)
{
    int ret = 0;

    write_log(DEBUG_LOG, "Sending error response: %d\n", code);

    // TODO: add error response payload
    http_response_t* response = http_response_new(code);
    ret = http_send_response(client, response);
    if (ret < 0)
        goto exit;
exit:
    http_response_free(response);
    return ret;
}

int http_send_response(const client_t* client, http_response_t* response)
{
    int ret = -1;

    if (response->payload_fd > 0) {
        int file_size = get_file_size(response->payload_fd);
        if (file_size < 0)
            return -1;

        if (file_size < 10000) {
            char size[10];
            snprintf(size, 10, "%d", file_size);
            if (http_add_response_property(response, "Content-Length", size) < 0)
                goto exit;

            write_log(DEBUG_LOG, "Sending response with length: %s\n", size);

            ret = http_send_response_normal(client, response, file_size);
        } else {
            /* inform the client that data is chunked */
            if (http_add_response_property(response, "Transfer-Encoding", "chunked") < 0)
                goto exit;

            write_log(DEBUG_LOG, "Sending response by chunks.\n");

            ret = http_send_response_chunked(client, response);
        }
    }

exit:
    return ret;
}
