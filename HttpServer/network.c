#include "network.h"

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "config.h"
#include "log.h"

static int socket_create(server_t* server, const char* interface)
{
    int err = 0;
    int opt_on = 1;

    err = (server->listen_fd = socket(AF_INET6, SOCK_STREAM, 0));

    if (err < 0) {
        write_log(ERROR_LOG, "Failed to create socket.");
        return err;
    }

    err = setsockopt(server->listen_fd, SOL_SOCKET, SO_REUSEADDR,
                     (void*) &opt_on, sizeof(opt_on));

    if (err < 0) {
        write_log(ERROR_LOG, "Failed to set socket as reusable.");
        return err;
    }

    if (interface != NULL && strlen(interface) > 0) {
        err = setsockopt(server->listen_fd, SOL_SOCKET, SO_BINDTODEVICE, interface, strlen(interface) + 1);  // with tailing \0
        if (err < 0) {
            write_log(ERROR_LOG, "Could not bind to interface \'%s\', error is: %s", interface, strerror(errno));
        }
    }


    return err;
}

static int socket_bind(const server_t* server, int port)
{
    int err = 0;
    struct sockaddr_in6 server_addr = { 0 };

    server_addr.sin6_family = AF_INET6;
    server_addr.sin6_addr = in6addr_any;
    server_addr.sin6_port = htons(port);

    err = bind(server->listen_fd,
               (struct sockaddr*)&server_addr,
               sizeof(server_addr));

    return err;
}

static int socket_listen(const server_t* server)
{
    const int BACKLOG = 4;
    int err = listen(server->listen_fd, BACKLOG);
    return err;
}

int socket_accept(const server_t* server, client_t* client)
{
    int result = 0;
    
    struct sockaddr_in6 client_addr;
    socklen_t client_len = sizeof(client_addr);
    
    struct sockaddr_in6 host_addr;
    socklen_t host_len = sizeof(host_addr);

    result = accept(server->listen_fd, (struct sockaddr*)&client_addr, &client_len);

    if (result < 0) {
        write_log(ERROR_LOG, "Failed to accept the connection.");
        return result;
    }

    /* retrieve the client address and port number */
    getpeername(result, (struct sockaddr *)&client_addr, &client_len);
    if(inet_ntop(AF_INET6, &client_addr.sin6_addr, client->client_address, sizeof(client->client_address))) {
        client->client_port = ntohs(client_addr.sin6_port);
        write_log(INFO_LOG, "Client connected: %s port %d\n", client->client_address, client->client_port);
    }
    
    /* retrieve the server address and port number */
    getsockname(result, (struct sockaddr*)&host_addr, &host_len);
    if(inet_ntop(AF_INET6, &host_addr.sin6_addr, client->host_address, sizeof(client->host_address))) {
        client->host_port = ntohs(host_addr.sin6_port);
        write_log(INFO_LOG, "Server receive connection at: %s port %d\n", client->host_address, client->host_port);
    }

    client->fd = result;

    return 0;

}

int socket_init(server_t* server, const config_t* config)
{
    int err = 0;

    err = socket_create(server, config->interface);
    if (err < 0) {
        write_log(ERROR_LOG,"Failed to create server\n");
        goto error;
    }

    err = socket_bind(server, atoi(config->port));
    if (err < 0) {
        write_log(ERROR_LOG,"Failed to bind socket to address.\n");
        goto error;
    }

    err = socket_listen(server);
    if (err < 0) {
        write_log(ERROR_LOG, "Failed to listen on port %s.\n", config->port);
        goto error;
    }

    return 0;

error:
    if (server->listen_fd > 0) {
        close(server->listen_fd);
    }

    return err;
}

/* make sure buff has enough length */
int socket_recv(const client_t* client, char* buff, int len)
{
    return recv(client->fd, buff, len, 0);
}
