#ifndef LOG_INCLUDE_
#define LOG_INCLUDE_

#include "http_protocol.h"

extern int log_level;

typedef enum log_level {
    ERROR_LOG = 3,
    WARNING_LOG = 4,
    INFO_LOG = 6,
    DEBUG_LOG = 7,
    TRACE_LOG = 8
} log_level_e;

/**
 * @brief Writes a log in custom format
 * @param level - log level (ERROR_LOG, WARNING_LOG, INFO_LOG, DEBUG_LOG, TRACE_LOG)
 * @param format - formatted string using 'format specifiers'
 */
void write_log(log_level_e level, const char* format, ...);

/**
 * @brief Writes a log in W3C format when a request is received
 * @param source - clients ip address (or whatever)
 * @param destination - hosts ip address (or whatever)
 * @param request - received request
 */
void write_access_log(const char* source_addr, const char* destination_addr, const http_request_t* request);

#endif //LOG_INCLUDE_
