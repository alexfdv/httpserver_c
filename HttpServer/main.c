#include <ctype.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <unistd.h>

#include "network.h"
#include "config.h"
#include "http_protocol.h"
#include "http_transport.h"
#include "http_handlers.h"
#include "utils.h"
#include "log.h"


typedef struct thread_context {
    int thread_number;
    client_t connected_client;
    config_t* config;
} thread_context_t;



/**
 * @brief Dispatches a request to the appropriate handler.
 * @param client - connected client, request - original request, conf - configuration file.
 */
static int handle_http_request(const client_t* client, const http_request_t* request, const config_t* conf)
{
    switch (request->method) {
    case GET:
        return handle_get_request(client, request, conf);
    default:
        return handle_default(client);
    }

    return 0;
}

/**
 * @brief Thread function that processes every connection.
 * @param data - thread context that contains useful for current connection information.
 */
static void* connection_handler(void* data)
{
    int ret = 0;

    thread_context_t* context = (thread_context_t*) data;
    int fd = context->connected_client.fd;

    // TODO: implement check for the payload size:
    // in case if there is not enough data for payload (read size from the header)
    // we need to increase buffer, read more data and then try to parse it again.
    // 1024 bytes is enough for GET request according to the "Simple HTTP server" task
    const int buf_len = 1024;
    char buffer[buf_len];
    memset(buffer, 0, buf_len);

    int bytes_read = 0;
    int consumed_bytes = 0;
    do {
        int bytes_remaining = buf_len - bytes_read;
        int received = socket_recv(&context->connected_client, buffer + bytes_read, bytes_remaining-1); //leave the last symbol \0
        if (received < 0) {
            write_log(ERROR_LOG, "Error occurred: %d", errno);
            break;
        } else if (received == 0) {
            write_log(DEBUG_LOG, "Connection is closed.");
            break;
        }

        bytes_read += received;
        bytes_remaining = buf_len-bytes_read;

        write_log(TRACE_LOG, "Received %d bytes: %s", received, buffer);
        write_log(TRACE_LOG, "%d read, %d remaining", bytes_read, bytes_remaining);

        http_request_t request;
        int parsed = try_parse_http_request(buffer, &request);
        if (parsed == 0 && bytes_remaining > 1) {
            write_log(INFO_LOG, "Need more data to read.");
            continue;
        } else if (parsed < 0 || (parsed == 0 && bytes_remaining <= 1)) {
            write_log(ERROR_LOG, "Bad HTTP request");
            http_send_error_response(&context->connected_client, BAD_REQUEST);
            write_log(DEBUG_LOG, "Moving buffer back to %d bytes with len %d.", bytes_read, bytes_remaining);
            consumed_bytes = bytes_read;
            goto reset_buffer;
        }
        
        consumed_bytes = parsed;
        
        write_log(TRACE_LOG, "Parsed %d bytes.", parsed);

        /* write access log in w3c format */
        write_access_log(context->connected_client.client_address, context->connected_client.host_address, &request);

        ret = handle_http_request(&context->connected_client, &request, context->config);
        bool keep_alive = request.keep_alive;
        http_request_free(&request);
        if (ret < 0) {
            write_log(ERROR_LOG, "Could not handle a request. Error = %d", ret);
            http_send_error_response(&context->connected_client, NOT_FOUND);
            consumed_bytes = parsed;
            goto reset_buffer;
        }

reset_buffer:
        write_log(TRACE_LOG, "Moving buffer back to %d bytes with remaining %d.", consumed_bytes, buf_len - consumed_bytes);
        memmove(buffer, buffer + consumed_bytes, buf_len - consumed_bytes);
        bytes_read = 0;

        /* close the connection if keep-alive is false */
        //if (!keep_alive) {
        //    break;
        //}
    } while(1);

    shutdown(fd, SHUT_RDWR);
    int err = close(fd);
    if (err) {
        write_log(ERROR_LOG, "Failed to close fd: %d", fd);
    }

    /* free thread context */
    free(context);

    write_log(INFO_LOG, "Closed the connection %d", fd);

    //fflush(stdout);
    return NULL;
}

/**
 * @brief Setup loggin
 * @param config - config file
 */
static void setup_logging(const config_t* config)
{
    log_level = TRACE_LOG;

    if (!config->enabled_logging) {
        log_level = 0;
    }
}

int main(int argc, char **argv)
{
    if (argc < 2) {
        write_log(ERROR_LOG, "Provide path to config using -c option.\n");
        exit(1);
    }

    /* parse a command line parameters */
    int c;
    char *cvalue = NULL;
    while ((c = getopt (argc, argv, "c:")) != -1) {
        switch (c) {
        case 'c':
            cvalue = optarg;
            break;
        case '?':
            if (optopt == 'c')
                write_log(ERROR_LOG, "Option -%c requires an argument.\n", optopt);
            else if (isprint (optopt))
                write_log(ERROR_LOG, "Unknown option `-%c'.\n", optopt);
            else
                write_log(ERROR_LOG, "Unknown option character `\\x%x'.\n", optopt);
            exit(1);
        default:
            exit(1);
        }
    }

    config_t* config = load_config(cvalue);
    if (!config) {
        write_log(ERROR_LOG, "Could not parse a config: %s\n", cvalue);
        exit(1);
    }

    setup_logging(config);

    /* check for default html */
    if (!check_file_exists(config->default_document_path)) {
        write_log(ERROR_LOG, "Default document does not exist: %s\n", config->default_document_path);
        exit(1);
    }

    write_log(INFO_LOG, "Config was successfully parsed.\n");

    int err = 0;
    server_t server = { 0 };

    err = socket_init(&server, config);
    if (err < 0) {
        goto error;
    }

    /* Loop to accept connections */
    for (;;) {
        client_t client = { 0 };
        int err = socket_accept(&server, &client);
        if (err < 0) {
            write_log(ERROR_LOG, "Failed to accept the connection\n");
            continue;
        }

        write_log(INFO_LOG, "Accepted a connection %d\n", client.fd);

        /* create a context for a new thread */
        thread_context_t* thread_ctx = (thread_context_t*)malloc(sizeof(thread_context_t));
        if (thread_ctx == NULL) {
            write_log(ERROR_LOG, "Could not create thread context.");
            continue;
        }
        thread_ctx->connected_client = client;
        thread_ctx->config = config;

        // TODO: add counting and refuse the connection if there is a lot of threads
        /* fork() can be used as well, but I decided to use threads. */
        pthread_t io_thread;
        err = pthread_create(&io_thread,  NULL, connection_handler,  thread_ctx) != 0;
        if (err != 0) {
            write_log(ERROR_LOG, "Failed to create a thread. Error: %d\n", err);
            continue;
        }
        pthread_detach(io_thread);
    }

error:
    write_log(ERROR_LOG, "Error occurred: %d\n", err);

    if (server.listen_fd > 0) {
        close(server.listen_fd);
    }

    return err;
}
